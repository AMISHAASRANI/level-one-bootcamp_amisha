#include <stdio.h>
int input()
{
    int a; 
    printf("Enter a number\n");
    scanf("%d",&a);
    return a;
}

int calc_sum(int a, int b)
{
    int sum;
    sum = a+b;
    return sum;
}

void output(int a, int b, int c)
{
    printf("Sum of %d and %d is %d\n",a,b,c);
}

int main()
{
    int num1,num2,sum;
    num1=input();
    num2=input();
    sum=calc_sum(num1,num2);
    output(num1,num2,sum);
    return 0;
}