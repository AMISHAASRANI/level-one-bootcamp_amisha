#include <stdio.h>
struct fraction
{
    int num;
    int den;

};
typedef struct fraction Fract;

Fract input()
{
    Fract f;
    printf("enter numerator");
    scanf("%d",&f.num);
    printf("enter denominator");
    scanf("%d",&f.den);
    return f;
}

int gcd(int a, int b) {
    int i,gcd=1;
    for(i=2; i <= a && i <= b; ++i)
    {
        if(a%i==0 && b%i==0)
            gcd = i;
    }
    return gcd;
}
int lcm(Fract arr[],int t)
{
    int ans;
    ans=arr[0].den;
    for(int i=1;i<t;i++)
        ans=(ans*arr[i].den)/gcd(ans,arr[i].den);
    return ans;
}
Fract add_frac(Fract arr[],int fd,int t){
    Fract sum;
    sum.num=0;
    for(int i=0;i<t;i++)
        sum.num=sum.num +arr[i].num*(fd/arr[i].den);
    sum.den=fd;
    return sum;
}

void output(Fract sum)
{
    if(sum.num==0)
        printf("Sum = 0\n");
    else if(sum.num==sum.den)
        printf("Sum = 1\n");
    else
        printf("Sum = %d/%d\n",sum.num,sum.den);
}

// Driver program
int main()
{
    Fract sum;
    int t;
    printf("Enter the number of Fractions to be entered:\n");
    scanf("%d",&t);
    Fract array[t];
    for(int i=0;i<t;i++)
        array[i]=input();
    int fd=lcm(array,t);
    sum=add_frac(array,fd,t);
    output(sum);
    return 0;
}
